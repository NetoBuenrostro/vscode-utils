"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require("vscode");
const myUtils = require("./vscode-util-commands");
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {
    myUtils.commandList.forEach(command => {
        let disposable = vscode.commands.registerTextEditorCommand(command.name, command.command);
        context.subscriptions.push(disposable);
    });
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map