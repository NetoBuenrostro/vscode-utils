import * as vscode from 'vscode';

export const commandList = [
    {
        name: 'extension.vscode-utils.reverseString',
        command: reverseString
    },
    {
        name: 'extension.vscode-utils.reverseDotSegment',
        command: reverseDotSegment
    }
];

/**
 * Reverses a string
 */
function reverseString(textEditor: vscode.TextEditor, edit: vscode.TextEditorEdit, ...args: any[]) {
    reverseSegment(textEditor, edit, '');
}

function reverseDotSegment(textEditor: vscode.TextEditor, edit: vscode.TextEditorEdit, ...args: any[]) {
    reverseSegment(textEditor, edit, '.');
}

function reverseSegment(textEditor: vscode.TextEditor, edit: vscode.TextEditorEdit, separator: string) {
    if (!hasSelection(textEditor)) {
        return;
    }

    // get the selected text
    var text = textEditor.document.getText(textEditor.selection);
    var reversedText = text.split("\n").map((value)=>value.split(separator).reverse().join(separator)).join("\n");
    // Replace the selected text
    edit.replace(textEditor.selection, reversedText);
}

function hasSelection(textEditor: vscode.TextEditor) {
    // if there isn't a selection, exit
    if (!textEditor || textEditor.selection.isEmpty) {
        vscode.window.showInformationMessage("This command only works with selected text");

        return false;
    }

    return true;
}
